import { ingredientType } from "./IngredientType";
import { recipeType } from "./RecipeType";

const types = [
  ingredientType,
  recipeType,
];

export default types;
