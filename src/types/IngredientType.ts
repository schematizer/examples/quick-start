import { Type } from '@schematizer/schematizer';

export class Ingredient {
  id: string;
  name: string;
  quantity: number;
};

export const ingredientType = new Type(Ingredient).as('IngredientType').fields(() => ({
  id: 'id',
  name: 'string',
  quantity: 'float',
}));
