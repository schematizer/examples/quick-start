import { Type } from "@schematizer/schematizer";
import { Ingredient } from "./IngredientType";

export class Recipe {
  id: string;
  title: string;
  ingredients: Ingredient[];
}

export const recipeType = new Type(Recipe).as('RecipeType').fields(() => ({
  id: 'id',
  title: 'string',
  ingredients: [Ingredient],
}));
