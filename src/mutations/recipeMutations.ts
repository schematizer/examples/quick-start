import { Mutations, Fn, Arg } from '@schematizer/schematizer';
import { Recipe } from '../types/RecipeType';
import { addRecipe } from '../core/index.ts';
import { RecipeInput } from '../inputs/RecipeInput';

export const recipeMutations = new Mutations({
  recipeCreate: Fn(Recipe, ({
    input = Arg(RecipeInput),
  }) => () => {
    return addRecipe(input);
  }),
});
