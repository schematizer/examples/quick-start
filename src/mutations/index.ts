import { recipeMutations } from "./recipeMutations";

const mutations = [
  recipeMutations,
];

export default mutations;
