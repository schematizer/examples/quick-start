import { Recipe } from "../types/RecipeType";
import { RecipeInput } from "../inputs/RecipeInput";

const recipes: Recipe[] = [];

export function getRecipes() {
  return recipes;
}

export function getRecipe(id: string) {
  return recipes.find(recipe => recipe.id === id);
}

export function addRecipe(recipeInfo: RecipeInput) {
  const recipe: Recipe = {
    id: `${recipes.length}`,
    title: recipeInfo.title,
    ingredients: recipeInfo.ingredients.map((ingredient, index) => ({
      id: `${recipes.length}-${index}`,
      ...ingredient,
    })),
  };

  recipes.push(recipe);

  return recipe;
}
