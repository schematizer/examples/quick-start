import { Queries, Fn, Arg } from "@schematizer/schematizer";
import { Recipe } from "../types/RecipeType";
import { getRecipes, getRecipe } from "../core/index.ts";

export const recipeQueries = new Queries({
  recipes: Fn([Recipe], () => () => {
    return getRecipes();
  }),

  recipe: Fn(Recipe, ({
    id = Arg('id'),
  }) => () => {
    return getRecipe(id);
  })
});
