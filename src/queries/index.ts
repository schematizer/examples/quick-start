import { recipeQueries } from "./recipeQueries";

const queries = [
  recipeQueries,
];

export default queries;
