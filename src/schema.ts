import types from "./types";
import { schematize } from "@schematizer/schematizer";
import inputs from "./inputs";
import queries from "./queries";
import mutations from "./mutations";

const schema = schematize(
  ...types,
  ...inputs,
  ...queries,
  ...mutations,
);

export default schema;
