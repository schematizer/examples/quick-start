import { ApolloServer } from 'apollo-server-express';
import * as express from 'express';
import schema from './schema';

const app = express();
const server = new ApolloServer({ schema });

server.applyMiddleware({ app });

app.listen({ port: 4545 }, () => {
  console.log(`🚀 Server ready at http://localhost:4545${server.graphqlPath}`)
});
