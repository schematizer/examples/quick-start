import { recipeInput } from "./RecipeInput";
import { ingredientInput } from "./IngredientInput";

const inputs = [
  recipeInput,
  ingredientInput,
];

export default inputs;