import { IngredientInput } from "./IngredientInput";
import { Input } from "@schematizer/schematizer";

export class RecipeInput {
  title: string;
  ingredients: IngredientInput[];
}

export const recipeInput = new Input(RecipeInput).fields(() => ({
  ingredients: [IngredientInput],
  title: 'string',
}));
