import { Input } from "@schematizer/schematizer";

export class IngredientInput {
  name: string;
  quantity: number;
};

export const ingredientInput = new Input(IngredientInput).fields(() => ({
  name: 'string',
  quantity: 'float',
}));
