# Quick start

### Download and run project
```bash
git clone https://gitlab.com/schematizer/examples/quick-start
cd quick-start
yarn
yarn serve
```

### Execute a mutation
```graphql
mutation Create {
  recipeCreate(input: {
    title: "Pizza"
    ingredients: [
      { name: "Foo", quantity: 1 },
      { name: "Bar", quantity: 2 },
    ]
  }) {
    id
    title
    ingredients {
      id
      name
      quantity
    }
  }
}
```

### Execute a query
```graphql
query Show {
  recipes {
    id
    title
    ingredients {
      id
      name
      quantity
    }
  }
}
```